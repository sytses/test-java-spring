This project is created from a GitLab [Project Template](https://docs.gitlab.com/ce/gitlab-basics/create-project.html)

Additions and changes to the project can be proposed [on the original project](https://gitlab.com/gitlab-org/project-templates/spring)

This was used [in a demo](https://docs.google.com/document/d/1WZjRhLZV7jjzeL9TR5bubxSSXBMDELtaqKWzW0eTPRs/edit) with 86 people watching from GitLab and more on Youtube.
